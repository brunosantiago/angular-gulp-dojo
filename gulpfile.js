var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var watch = require('gulp-watch');
var wiredep = require('wiredep').stream;
var inject = require('gulp-inject');

// Server
gulp.task('server', ['inject'], function () {
  browserSync.init({
      open: true,
      notify: false,
      port: 3000,
      server: {
          baseDir: './public'
      }
  });
  gulp.start('watch');
});

// Watch
gulp.task('watch', function() {
  var sources_js = './public/**/*.js';
  var sources_css = './public/**/*.css';
  var sources_html = './public/**/*.html';

  watch(sources_js, function() {
      gulp.start(['inject','browserSync']);
  });

  watch(sources_css, function() {
      gulp.start(['inject','browserSync']);
  });

  watch(sources_html, function() {
      gulp.start(['browserSync']);
  });
});

gulp.task('browserSync', function() {
    browserSync.reload();
});

// Inject
gulp.task('inject', function () {
  var baseDir = './public';
  var sources_js = ['./public/**/*.js', '!./public/bower_components/**/*.js'];
  var sources_css = ['./public/**/*.css', '!./public/bower_components/**/*.css'];
  var wiredepOptions = { directory: './public/bower_components/' };

  var options = {
      read: false,
      ignorePath: ['public'],
      addRootSlash: false
  };

  return gulp.src('./public/index.html')
      .pipe(inject(gulp.src(sources_js), options))
      .pipe(inject(gulp.src(sources_css), options))
      .pipe(wiredep(wiredepOptions))
      .pipe(gulp.dest(baseDir));
});
